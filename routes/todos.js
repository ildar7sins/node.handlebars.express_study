const {Router} = require('express');
const ToDo = require('../models/ToDo');
const router = Router();

router.get('/', async (req, res) => {
	let todos = await ToDo.find({}).lean();
	
	res.render('index', {
		title: 'ToDos',
		isIndex: true,
		todos
	});
});

router.get('/create', (req, res) => {
	res.render('create', {
		title: 'Create ToDo',
		isCreate: true
	});
});

router.post('/create', async (req, res) => {
	const todo = new ToDo({
		title: req.body.title
	});
	
	await todo.save();
	
	res.redirect('/')
});

router.post('/complete', async (req, res) => {
	const todo = await findModel(req.body.id, res);
	
	todo.completed = !!req.body.completed;
	await todo.save();
	
	res.redirect('/')
});

const findModel = async (id, res) => {
	try {
		if(id){
			const todo = await ToDo.findById(id);
			if(todo){
				return todo;
			}
		}
	} catch (e){
		res.status(404).send('ToDo not found!');
	}
	
	res.status(404).send('ToDo not found!');
}


module.exports = router;
