const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const exphbs = require('express-handlebars');
const toDoRoutes = require('./routes/todos');

const PORT = process.env.PORT || 3000;

const app = express();

// Add handlebars engine...
const hbs = exphbs.create({defaultLayout: 'main', extname: 'hbs'});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

// Midleware for work with body...
app.use(express.urlencoded({extended: true}));

// Add static files...
app.use(express.static(path.join(__dirname, 'public')))

// Routes...
app.use(toDoRoutes);

// Express server start wrapper...
async function start() {
	try {
		await mongoose.connect('mongodb+srv://root:root@cluster0.ctiuq.mongodb.net/todos', {
			useNewUrlParser: true,
			useFindAndModify: false
		});
		
		app.listen(PORT, () => {
			console.log('Server has been started...')
		});
	} catch (e) {
		console.log(e);
	}
}


start();
